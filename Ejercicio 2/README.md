### Pasos

##### Ejercicio 2
1. Utilizar dig.
2. identificar la dirección IP de cada uno de los sitios.
3. Utilizar la herramienta geoip.
4. Identificar la geolocalización de cada dirección IP.
5. Utilizando nmap, obtener cualquier información adicional, como puertos abiertos.


1. Instalar dig  
```
dig vulnweb.com +noall +answer
```
![Paso 1](image.png)

2. Identificar la dirección IP de cada uno de los sitios
```
dig vulnweb.com +noall +answer
``` 

3. Utilizar la herramienta geoip
![Instalando geoip](image-1.png)

4. Identificar la geolocalización de cada dirección IP
``` 
geoiplookup 44.228.249.3 
```
![Ubicacion del sitio](image-2.png)

5. Utilizando nmap, obtener cualquier información adicional, como puertos abiertos
```
nmap -sV -p 80 44.228.249.3
```
![Puerto 80](image-4.png)

![Deteccion del OS](image-5.png)

### Conclusiones
- El sitio tiene el puertos 80 abierto.
- El sitio no tiene un certificado SSL valido.
- El sitio tiene un servidor web Nginx 1.19.0 funcionando.
- Si el sitio fuera un Nginx en docker, tendria estas vulnerabilidades
https://hub.docker.com/layers/library/nginx/1.19/images/sha256-57201d91b3f356bbde8f631613eda851c9d552edcfb3ba3dd3fcd3feb5278fa1?context=explore

