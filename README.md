## TP-11

#### Práctica sobre Seguridad IT:

Buscamos como objetivo principal, acercarnos al uso de herramientas y buenas
practicas en ciber-seguridad, este TP esta orientada a las primeras fases de un pentesting.

**Fecha de entrega: Miercoles 27/12/2023**

#### Consignas:

##### Ejercicio 1
1. Utilizar dig y whois
2. Obtener la información del dominio vulnweb.com.
3. Utilizar Google
4. Identificar qué sitios web están hosteados en vulnweb.com

##### Ejercicio 2
1. Utilizar dig.
2. identificar la dirección IP de cada uno de los sitios.
3. Utilizar la herramienta geoip.
4. Identificar la geolocalización de cada dirección IP.
5. Utilizando nmap, obtener cualquier información adicional, como puertos abiertos.

##### Entregables
Como forma de entrega de cada uno de estos ejercicios, necesitamos imágenes o pdf que muestren las salidas de sus análisis y una breve conclusión (pdf) indicando cual seria una posible superficie de ataque.