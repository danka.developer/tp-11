### Pasos
1. Instalar dig  
![Paso 1](image.png)

2. Obtener la información del dominio vulnweb.com
![Paso 2](image-1.png)
![Paso 2 whois](image-2.png)

3. Utilizar Google

4. Identificar qué sitios web están hosteados en vulnweb.com
dig vulnweb.com +noall +answer
![Paso 4](image-3.png)
